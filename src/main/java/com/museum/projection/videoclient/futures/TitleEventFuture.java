package com.museum.projection.videoclient.futures;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TitleEventFuture implements Future<String> {


    private volatile static String receivedTitle = "";

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return !receivedTitle.equals("");
    }

    @Override
    public String get() throws InterruptedException, ExecutionException {
        String toSend = receivedTitle;
        receivedTitle = "";
        return toSend;
    }

    @Override
    public String get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        String toSend = receivedTitle;
        receivedTitle = "";
        return toSend;
    }

    public static void onTitle(String title) {
        receivedTitle = title;
    }


}
