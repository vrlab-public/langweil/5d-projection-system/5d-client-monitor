package com.museum.projection.videoclient;

import com.museum.projection.videoclient.configuration.CustomConfiguration;
import com.museum.projection.videoclient.futures.TitleEventFuture;
import com.museum.projection.videoclient.mplayer.RemoteClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


@Service
@EnableScheduling
public class EventService {


    private static final Logger log = LoggerFactory.getLogger(EventService.class);

    //    private static final String DEFAULT_PLAYLIST = "/home/otrojan/Documents/Projects/Diplomka/loop";
//        private static final String DEFAULT_PLAYLIST = "/home/otrojan/Documents/Projects/Diplomka/my";
    private static final String DEFAULT_PLAYLIST = "/home/otrojan/Documents/Projects/Diplomka/playlistMaster";


    static private volatile boolean servingRequest;

    private boolean verifiedStartup = false;
    private LocalDateTime lastCommand;

    @Resource
    private OutboundService outboundService;

    @Resource
    private CustomConfiguration configuration;

    /**
     * @param displaySetId
     * @param principal
     * @param model
     * @return
     */
    @Resource(name = "player")
    public RemoteClient mplayerClient;

    public EventService() {
    }


    public String getTitle() throws ExecutionException, InterruptedException {
        Future<String> future = new TitleEventFuture();
        mplayerClient.sendCmd("get_file_name\n");
        while (!future.isDone()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return future.get();
    }

    public void next() {
        mplayerClient.sendCmd("pausing_keep_force pt_step 1\n");
    }

    public void next(String value) {
        mplayerClient.sendCmd("pausing_keep_force pt_step " + value + "\n");
    }

    public void play() {
        if (mplayerClient.isPlaying()) return;
        togglePause();
    }

    public void seek(String value, String type) {
        mplayerClient.sendCmd("seek " + value + " " + type + "\n");
    }


    public void togglePause() {
        mplayerClient.sendCmd("p\n");
    }

    public void stop() {
        mplayerClient.sendCmd("stop\n");
    }


    public void close() {
        mplayerClient.close();
    }

    int upMessageCounter = 0;

    @Scheduled(fixedDelay = 1000)
    public void sendInfoAboutBeingUp() {
//        upMessageCounter++;
//        if (upMessageCounter >= configuration.getUpMessageInterval()) {
//            upMessageCounter = 0;
//            verifiedStartup = false;
//        }

        //init
        if (!verifiedStartup) {
            ResponseEntity<String> response = outboundIntermediatePost("up/" + configuration.getId());
            if (response != null && response.getStatusCode().is2xxSuccessful()) {
                verifiedStartup = true;
            }
            return;
        }

        //send status master
        if (!configuration.isMaster()) return;
        JSONObject json = new JSONObject();
        try {
            json.put("mplayerRuning", mplayerClient.isAlive());
            if (mplayerClient.isAlive()) {
                json.put("playing", mplayerClient.isPlaying());
                json.put("playedTrack", mplayerClient.getTitle());
                json.put("currentTime", mplayerClient.getPosition());
                json.put("trackDuration", mplayerClient.getDuration());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ResponseEntity<String> response = outboundIntermediatePost("status/" + configuration.getId(), json);
        if (response != null) {
            log.debug(response.toString());
        }
    }


    public synchronized boolean restart(String playlist) {
        if (mplayerClient.isAlive()) {
            close();
        }
        return mplayerClient.start(playlist);
    }


    public static void setServingRequest(boolean servingRequest) {
        EventService.servingRequest = servingRequest;
    }


    public static boolean isServingRequest() {
        return servingRequest;
    }

    private ResponseEntity<String> outboundIntermediatePost(String msg) {
        return outboundIntermediatePost(msg, null);
    }

    private ResponseEntity<String> outboundIntermediatePost(String msg, JSONObject json) {
        ResponseEntity<String> response = outboundService.postCmd(msg, json);
        if (response == null) verifiedStartup = false;
        return response;
    }


}
