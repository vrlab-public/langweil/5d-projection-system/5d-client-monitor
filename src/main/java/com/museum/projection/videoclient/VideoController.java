package com.museum.projection.videoclient;

import com.museum.projection.videoclient.configuration.CustomConfiguration;
import com.museum.projection.videoclient.configuration.PlayerRuntimeConfig;
import com.museum.projection.videoclient.mplayer.RemoteClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

@RestController
public class VideoController {

    @Resource(name = "player")
    public RemoteClient mplayerClient;

    @Resource
    public CustomConfiguration customConfiguration;

    @Resource
    public EventService eventService;

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/test")
    String test() {
        return "endpoint running";
    }

    @PostMapping(value = "/add")
    ResponseEntity<String> runMplayer(@RequestBody PlayerRuntimeConfig runtimeConfig) {
        //TODO sanitate inputs
        System.out.println(runtimeConfig.getPlaylist());
        boolean started = eventService.restart(runtimeConfig.getPlaylist());
        if (started) return new ResponseEntity<>("Start successful", HttpStatus.OK);
        return new ResponseEntity<>("Start failed", HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @PostMapping("/next")
    ResponseEntity<String> next() {
        System.out.println("Receving next ");
        if (customConfiguration.isMaster()) {
            EventService.setServingRequest(true);
        }
        eventService.next();
        return new ResponseEntity<>(
                "Executed command next", HttpStatus.OK);
    }

    @PostMapping("/next/{value}")
    ResponseEntity<String> nextValue(@PathVariable("value") String value) {
        //TODO sanitate inputs
        System.out.println("Received controller next " + value);
        if (customConfiguration.isMaster()) {
            EventService.setServingRequest(true);
        }
        eventService.next(value);
        return new ResponseEntity<>(
                "Executed command next", HttpStatus.OK);
    }


//    *****  Master Specific  *****

    @GetMapping("/title")
    ResponseEntity<String> title() {
        System.out.println("Received controller title");
        String title = null;
        try {
            title = eventService.getTitle();
            return new ResponseEntity<>(
                    "Player executed command title: " + title, HttpStatus.OK);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return new ResponseEntity<>(
                    "Failed: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/seek/{value}/{type}")
    ResponseEntity<String> seek(@PathVariable("value") String value, @PathVariable("type") String type) {
        //TODO sanitate inputs
        System.out.println("Received controller seek");
        eventService.seek(value, type);
        return new ResponseEntity<>(
                "Executed command seek " + value + " " + type, HttpStatus.OK);
    }

    @PostMapping("/pause")
    ResponseEntity<String> pause() {
        eventService.togglePause();
        return new ResponseEntity<>(
                "Executed command pause", HttpStatus.OK);
    }

    @PostMapping("/play")
    ResponseEntity<String> play() {
        eventService.play();
        return new ResponseEntity<>(
                "Executed command play", HttpStatus.OK);
    }

    @PostMapping("/stop")
    ResponseEntity<String> stop() {
        eventService.stop();
        return new ResponseEntity<>(
                "Executed command stop", HttpStatus.OK);
    }

    @PostMapping("/shutdown")
    ResponseEntity<String> killMplayer() {
        eventService.close();
        return new ResponseEntity<>("Closed", HttpStatus.OK);
    }


}
