package com.museum.projection.videoclient.configuration;

import com.museum.projection.videoclient.mplayer.MplayerClient;
import com.museum.projection.videoclient.mplayer.RemoteClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Configuration
public class PlayerConfiguration {

    @Resource
    public CustomConfiguration customConfiguration;

    @Resource
    public RestTemplate restTemplate;

    @Bean("player")
    public RemoteClient initPlayer() {
        return new MplayerClient(customConfiguration,restTemplate);
//        System.out.println("Started after Spring boot application ! : " + customConfiguration);
    }
}
