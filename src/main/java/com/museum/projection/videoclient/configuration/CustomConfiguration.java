package com.museum.projection.videoclient.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties("videoclient")
public class CustomConfiguration {

    private Boolean master;
    private Boolean enable;
    private int id;
    private String port;
    private String ip;
    private String homeServerIp;
    private String homeServerPort;
    private String homeServerUsername;
    private String homeServerPassword;
    private List<NodeConfig> slaves = new ArrayList<>();


    public Boolean isMaster() {
        return master;
    }

    public void setMaster(Boolean master) {
        this.master = master;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<NodeConfig> getSlaves() {
        return slaves;
    }

    public void setSlaves(List<NodeConfig> slaves) {
        this.slaves = slaves;
    }

    public String getHomeServerIp() {
        return homeServerIp;
    }

    public void setHomeServerIp(String homeServerIp) {
        this.homeServerIp = homeServerIp;
    }

    public String getHomeServerPort() {
        return homeServerPort;
    }

    public void setHomeServerPort(String homeServerPort) {
        this.homeServerPort = homeServerPort;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHomeServerUsername() {
        return homeServerUsername;
    }

    public void setHomeServerUsername(String homeServerUsername) {
        this.homeServerUsername = homeServerUsername;
    }

    public String getHomeServerPassword() {
        return homeServerPassword;
    }

    public void setHomeServerPassword(String homeServerPassword) {
        this.homeServerPassword = homeServerPassword;
    }
}
