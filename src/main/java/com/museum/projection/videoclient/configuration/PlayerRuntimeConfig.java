package com.museum.projection.videoclient.configuration;

public class PlayerRuntimeConfig {

    private String playlist;

    public String getPlaylist() {
        return playlist;
    }

    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }
}
