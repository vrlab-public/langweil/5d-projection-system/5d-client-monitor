package com.museum.projection.videoclient.configuration;

public class NodeConfig {
    private String ip;
    private int mPlayerMonitorPort;


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }


    public int getmPlayerMonitorPort() {
        return mPlayerMonitorPort;
    }

    public void setmPlayerMonitorPort(int mPlayerMonitorPort) {
        this.mPlayerMonitorPort = mPlayerMonitorPort;
    }


}

