package com.museum.projection.videoclient.mplayer;

public class MplayerState {
    private String currentUri;
    private String position;
    private String duration;
    private String title;
    private boolean isPlaying;
    private int volumeInMillidels = -1;

    public void setCurrentUri(String currentUri) {
        this.currentUri = currentUri;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setVolume(int volumeInMillidels) {
        this.volumeInMillidels = volumeInMillidels;
    }

    public int getVolumeInMillidels() {
        return volumeInMillidels;
    }

    public String getStringPosition(){
        return position;
    }

    public double getPosition() {
        if (position == null) return 0;
        return Double.parseDouble(position);
    }

    public double getDuration() {
        if (duration == null) return 0;
        return Double.parseDouble(duration);
    }

    public String getCurrentUri() {
        return currentUri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }
}
