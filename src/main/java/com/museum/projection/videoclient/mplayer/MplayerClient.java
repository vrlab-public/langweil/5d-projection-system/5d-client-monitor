package com.museum.projection.videoclient.mplayer;

import com.museum.projection.videoclient.configuration.CustomConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Mplayer client
 */
public class MplayerClient implements RemoteClient {

    private static final Logger log = LoggerFactory.getLogger(MplayerClient.class);
    private static final int START_POSITION = 0;
    private static final String COMMAND_PAUSE = "p";
    private static final String COMMAND_QUIT = "q\n";
    private static final String OPTION_MEDIA_INFO = "-identify";
    private static final String COMMAND_FULLSCREEN = "-fs";
    private static final String COMMAND_INCREASE_VOLUME = "0";
    private static final String COMMAND_DECREASE_VOLUME = "9";
    private static final String OPTION_NO_VIDEO = "-novideo";
    private static final String OPTION_NO_AUDIO = "-ao null";
    private static final String OPTION_START_POSITION = "-ss";
    private static final int VERBOSITY = 2;

    private final CustomConfiguration configuration;
    private final RestTemplate restTemplate;

    private final MplayerState mplayerState;

    private MplayerOutputStreamConsumer stdOutConsumer;
    private Process playerMasterProcess = null;
    private PrintStream stdOut;

    private Thread cleanupThread = new Thread(this::destruct);


    private void destruct() {
        if (playerMasterProcess != null) {
            playerMasterProcess.destroy();

        }
//        close();
    }


    public MplayerClient(CustomConfiguration configuration, RestTemplate restTemplate) {
        this.configuration = configuration;
        this.mplayerState = new MplayerState();
        this.restTemplate = restTemplate;
//        start();

    }

    public boolean isAlive() {
        return playerMasterProcess != null && playerMasterProcess.isAlive();
    }


    @Override
    public boolean start(String playlist) {
        System.out.println("Is master " + this.configuration.isMaster());

//        Runtime.getRuntime().addShutdownHook(cleanupThread);
        //launch player
        List<String> cmd = new ArrayList<>(Collections.singletonList("mplayer"));
//        cmd.add(START_POSITION + "");
        cmd.add("-loop");
        cmd.add("0");

        cmd.add("-osdlevel");
        cmd.add("0");

        cmd.add("-slave");

        for (int i = 0; i < VERBOSITY; i++) {
            cmd.add("-v");

        }


        if (!configuration.isMaster()) {
            cmd.add(COMMAND_FULLSCREEN);
        }

        if (configuration.isMaster()) {
            cmd.add("-udp-master");
        } else {
            cmd.add("-udp-slave");
        }

        cmd.add("-udp-ip");
        cmd.add(configuration.getIp());

        cmd.add("-udp-port");
        cmd.add(configuration.getPort());
        cmd.add("-playlist");
        cmd.add(playlist);

        System.out.println(cmd);
        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        try {
            playerMasterProcess = processBuilder.start();
            InputStream stdIn = playerMasterProcess.getInputStream();
            InputStream errIn = playerMasterProcess.getErrorStream();
            stdOut = new PrintStream(playerMasterProcess.getOutputStream());


            this.stdOutConsumer = new MplayerOutputStreamConsumer(stdIn, mplayerState, new MplayerEventHandler(this,configuration.isMaster(), configuration.getSlaves() , restTemplate));
            Thread stdOutThread = new Thread(stdOutConsumer);
            stdOutThread.start();

//            errOutConsumer = new MPlayerErrorStreamConsumer(errIn);
//            Thread errOutThread = new Thread(errOutConsumer);
//            errOutThread.start();

        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return playerMasterProcess.isAlive();
    }


    public int close() {
        if (playerMasterProcess == null) return 0;

        if (stdOutConsumer != null) {
            stdOutConsumer.deactivate();
        }
//        errOutConsumer.deactivate();

        Runtime.getRuntime().removeShutdownHook(cleanupThread);


        sendCmd(COMMAND_QUIT);
        try {
            return playerMasterProcess.waitFor();
        } catch (InterruptedException iex) {
            log.error(iex.getMessage(), iex);
        }
        return 1;
    }


    public int getId() {
        //TODO fix exception
        throw new RuntimeException("asdsad");
    }


    public void sendCmd(String msg) {
        stdOut.print(msg);
        stdOut.flush();
    }

    public double getPosition(){
        return mplayerState.getPosition();
    }

    public String getTitle(){
        return mplayerState.getTitle();
    }

    public double getDuration(){
        return mplayerState.getDuration();
    }


    /**
     * @return
     */
    public boolean isConnected() {
        return false;
    }

    /**
     * @return
     */
    public boolean isPlaying() {
        return mplayerState.isPlaying();
    }


}
