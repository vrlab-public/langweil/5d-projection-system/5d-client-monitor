package com.museum.projection.videoclient.mplayer;

/**
 * Interface for remote clients
 */
public interface RemoteClient {
    /**
     * @return
     */
    public int getId();

    /**
     * @param msg
     * @return
     */
    public void sendCmd(String msg);


    /**
     * @return
     */
    public boolean isConnected();

    /**
     * @return
     */
    public boolean isPlaying();


    public int close();

    public boolean start(String playlist);

    boolean isAlive();

    public double getPosition();

    public String getTitle();

    public double getDuration();


}

