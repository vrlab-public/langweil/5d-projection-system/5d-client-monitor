package com.museum.projection.videoclient.mplayer;

import com.museum.projection.videoclient.EventService;
import com.museum.projection.videoclient.configuration.NodeConfig;
import com.museum.projection.videoclient.futures.TitleEventFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MplayerEventHandler implements PlayerEventHandler {

    private static final Logger log = LoggerFactory.getLogger(MplayerEventHandler.class);

    private final MplayerClient mplayerClient;

    private List<NodeConfig> slaves;
    private boolean isMaster;

    private RestTemplate restTemplate;
    private final String PROTOCOL = "http://";


    public MplayerEventHandler(MplayerClient mplayerClient, boolean isMaster, List<NodeConfig> slaves, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.mplayerClient = mplayerClient;
        this.isMaster = isMaster;
        this.slaves = slaves;
    }

    @Override
    public void onTrackEnded() {
        System.out.println("on track ended");
        if (isMaster) {
            if (slaves.isEmpty()) return;
            if (EventService.isServingRequest()) {
                System.out.println("Skipping");
                EventService.setServingRequest(false);
                return;
            }
            boolean ok = true;
            for (NodeConfig slave : slaves) {
                ResponseEntity<String> response = next(slave.getIp(), slave.getmPlayerMonitorPort());
                ok &= response != null && response.getStatusCode().is2xxSuccessful();
            }
            System.out.println(ok ? "Successfully" : "Unsuccessfully" + "sent to slaves ");
        }
    }


    public ResponseEntity<String> next(String ip, int port) {
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request =
                new HttpEntity<String>("", headers);
        System.out.println("Sending next to " + ip + ":" + port);
        try {
            response = restTemplate.postForEntity(PROTOCOL + ip + ":" + port + "/next", request, String.class); //master
            return response;

        } catch (RestClientException ex) {
            log.error("Slave " + ip + ":" + port + " refused connection", ex);
            return null;
        }
    }

    @Override
    public void onStaringPlayback() {
        System.out.println("on start");
    }

    @Override
    public void onPlayedTrack(String line) {
        System.out.println("playing " + line);
        setDuration();
    }

    @Override
    public void onFirstStart(String cmd) {
        System.out.println("on first start ");

    }

    @Override
    public void onTitle(String title) {
        TitleEventFuture.onTitle(title);
    }

    private void setDuration(){
        mplayerClient.sendCmd("get_time_length\n");
    }
}
