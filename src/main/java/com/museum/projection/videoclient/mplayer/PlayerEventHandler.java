package com.museum.projection.videoclient.mplayer;

import org.springframework.stereotype.Component;

@Component
public interface PlayerEventHandler {

    void onTrackEnded();

    void onStaringPlayback();

    void onPlayedTrack(String line);

    void onFirstStart(String cmd);

    void onTitle(String title);
}
