package com.museum.projection.videoclient.mplayer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MplayerOutputStreamConsumer implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(MplayerOutputStreamConsumer.class);

    private final static String PAUSE = "=====  PAUSE  =====";
    private final static String EXIT = "ID_EXIT=";
    private final static String END_OF_FILE = "EOF";
    private final static String QUIT = "QUIT";
    private final InputStream in;
    private final MplayerState mplayerState;
    private boolean started = false;
    //    private final PlayerEventHandler playerEventsHandler;
    private BufferedReader bufferedReader = null;
    //    private volatile boolean isExcluded = false;
    private volatile String title;


    private final PlayerEventHandler playerEventsHandler;

    private boolean active = true;

    public MplayerOutputStreamConsumer(
            final InputStream in,
            final MplayerState playerState,
            final PlayerEventHandler mplayerEventHandler) {
        this.in = in;
        this.mplayerState = playerState;
        this.playerEventsHandler = mplayerEventHandler;
    }

    public void run() {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(in));
            String line = null;
//            while (active) {
//                while (active && !isExcluded) {
            while (active && (line = getLine(bufferedReader)) != null) {
                //check if not
//                log.info(line);
                handlePause(line);
                handleTitle(line);
                handleStartingPlayback(line);
                handlePlaying(line);
                handlePosition(line);
                handleDuration(line);
                handleStreamEnded(line);

            }
//            }
        } catch (Exception exception) {
            log.error(exception.getMessage(), exception);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ioException) {
                log.error(ioException.getMessage(), ioException);
            }
        }
    }

    private void handlePause(String line) {
        mplayerState.setPlaying(!line.contains(PAUSE));
    }

    private void handleDuration(String line) {
        if (line.startsWith("ANS_LENGTH=")) {
            mplayerState.setDuration(line.replace("ANS_LENGTH=", ""));
        }
    }

    private void handleTitle(String line) {
        if (isStartingAsTitle(line)) {
            playerEventsHandler.onTitle(line.replace("ANS_FILENAME=", ""));
        }

    }

    private boolean isStartingAsTitle(String line) {
        return line.startsWith("ANS_FILENAME=");
    }


    synchronized private String getLine(BufferedReader bufferedReader) throws IOException {
        return bufferedReader.readLine();
    }

    //called from differend thread
//    public void handleResponse() {
//
////        isExcluded = true;
//
//        String line = "test";
//        try {
//            while ((line = getLine(bufferedReader)) != null
//                    && !isExitLine(line)
//                    && !isStartingPlaybackLine(line)
//                    && !isPositionLine(line)) {
//
//                System.out.println("Received 1:" + line);
//            }
//            System.out.println("Received 2:" + line);
////            isExcluded = false;
//
//        } catch (IOException exception) {
//            log.error(exception.getMessage(), exception);
//        } finally {
//            try {
//                bufferedReader.close();
//            } catch (IOException ioException) {
//                log.error(ioException.getMessage(), ioException);
//            }
//        }
//    }

    private void handleStartingPlayback(final String line) {
        if (isStartingPlaybackLine(line)) {
            playerEventsHandler.onStaringPlayback();
        }
    }

    private void handlePlaying(final String line) {
        if (isPlayingLine(line)) {
            if (!started) {
                playerEventsHandler.onFirstStart("");
                started = true;
            }
            String title = line.replace("Playing ", "");
            mplayerState.setTitle(title);
            playerEventsHandler.onPlayedTrack(title);
        }
    }

    private boolean isStartingPlaybackLine(final String line) {
        return line.startsWith("Starting playback");
    }

    private boolean isPlayingLine(final String line) {
        return line.startsWith("Playing");
    }

    private void handleStreamEnded(String line) {
        if (isTrackEndedLine(line)) {
            playerEventsHandler.onTrackEnded();
        }
    }

    public void deactivate() {
        active = false;
    }

    private void handlePosition(final String line) {
        if (isPositionLine(line)) {
            String[] split = line.trim().replaceAll("\\s{2,}", " ").split("\\s");
//            System.out.println(Arrays.toString(split));
            String position = split[1];
            mplayerState.setPlaying(!position.equals(mplayerState.getStringPosition()));
            mplayerState.setPosition(position);
        }
    }

    private boolean isPositionLine(String line) {
        return line.startsWith("A:");
    }

    private boolean isTrackEndedLine(String line) {
        if (isExitLine(line)) {
//            final String exitValue = line.trim().substring(EXIT.length());
//            return exitValue.equals(END_OF_FILE);
            return true;
        }
        return false;
    }

    private static boolean isExitLine(String line) {
        return line.startsWith(END_OF_FILE);
    }
}
