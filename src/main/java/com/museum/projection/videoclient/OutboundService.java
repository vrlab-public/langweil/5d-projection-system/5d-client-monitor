package com.museum.projection.videoclient;

import com.museum.projection.videoclient.configuration.CustomConfiguration;
import org.apache.tomcat.util.json.Token;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.kerberos.EncryptionKey;
import java.util.Base64;
import java.util.Objects;

@Service
public class OutboundService {

    private static final Logger log = LoggerFactory.getLogger(OutboundService.class);
    private RestTemplate restTemplate;
    private CustomConfiguration config;
    private final String PROTOCOL = "http://";
    private final String API_PREFIX = "/api/player";
    private String authToken;

    public OutboundService(RestTemplate restTemplate, CustomConfiguration config) {
        this.restTemplate = restTemplate;
        this.config = config;
    }


    public ResponseEntity<String> postCmd(String msg) {
        return postCmd(msg, null);
    }

    public ResponseEntity<String> postCmd(String msg, JSONObject json) {
        if (authToken == null) {
            if (!authenticate()) {
                log.error("cannot authenticate with credentials");
                return null;
            }
        }
        ResponseEntity<String> response;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request =
                new HttpEntity<String>(json == null ? "" : json.toString(), headers);
        log.info("POST: " + PROTOCOL + config.getHomeServerIp() + ":" + config.getHomeServerPort() + API_PREFIX + "/" + msg + (json == null ? "" : ", JSON: " + json.toString()));
        try {
            response = restTemplate.postForEntity(PROTOCOL + config.getHomeServerIp() + ":" + config.getHomeServerPort() + API_PREFIX + "/" + msg, request, String.class);
            log.debug(response.toString());
            return response;
        } catch (RestClientException ex) {
            log.error("Cannot connect to home server ", ex);
        }
        return null;
    }

    private boolean authenticate() {
        HttpHeaders headers = new HttpHeaders();
        JSONObject json = new JSONObject();
        try {
            json.put("username", config.getHomeServerUsername());
            json.put("password", config.getHomeServerPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HttpEntity<String> request =
                new HttpEntity<String>(json.toString(), headers);
        ResponseEntity<String> response;
        try {
            response = restTemplate.postForEntity(PROTOCOL + config.getHomeServerIp() + ":" + config.getHomeServerPort() + "/login", request, String.class); //maste
        } catch (RestClientException ex) {
            log.error("Error during authentication, ex");
            return false;
        }
        this.authToken = response.getHeaders().getFirst("Authorization");
        System.out.println("Received token: " + authToken);
        return response.getStatusCode().is2xxSuccessful() && !this.authToken.isEmpty();
    }

}
