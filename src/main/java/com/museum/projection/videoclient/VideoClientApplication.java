package com.museum.projection.videoclient;

import com.museum.projection.videoclient.configuration.CustomConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@SpringBootApplication
@EnableConfigurationProperties(CustomConfiguration.class)
public class VideoClientApplication {



    public static void main(String[] args) {
        System.out.println("bef");
        SpringApplication.run(VideoClientApplication.class, args);
        System.out.println("aft");
    }



    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
